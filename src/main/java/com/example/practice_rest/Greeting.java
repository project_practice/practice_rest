package com.example.practice_rest;

public record Greeting(long id, String content) {
}
